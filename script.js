console.log ("Hello World!");
// Naming Variables
const pi = 3.1416; //cannot be re-assigned
console.log (pi);

var name = "Lalaine";
console.log (name);

let name1 = "Regie";
console.log (name1);

//let name1 ="Paulo"
//console.log (name1); //let cannot be re-declared

//Primitive Data Types
var name = "Marco";
console.log (name); //var can be overwrite

let fruit = "Guava"; //String - words characters
let age = 15; //Integer/Number
let grade = 90.5;

console.log (fruit);
console.log (age);
console.log (grade);

let isPresent = true;
console.log(isPresent);

let myCopy;
console.log (myCopy); //nothing assigned that is why it is undefined.


let option1 = null;
console.log (option1);

//let 1fruit = apple;
//console.log (1fruit); // we do not apply a number first before a letter/word

let $year = 2022;
console.log ($year); //we can use special characters before word when naming a variable

let $_year = 2023;
console.log($_year); //we can use 2 special characters before word when naming a variable.

let state = `Washington`;
console.log(state);

//Objects

let person = {
    //property:value

    name: "Jose Rizal",//string
    age:30,
    address: "Calamba, Laguna", //string
    isDoctor:true, //boolean
    spouse:null,
    siblings: [ //array

        "Paciano",
        "Saturnina",
        "Narcisa",
        "Olympia",
        "Soledad",
        "Concepcion",
        "Maria",
        "Lucia",
        "Josefa",
        "Trinidad"

    ]

};

console.log (person);
console.log (person.name);
console.log (person.siblings); //use dot notation to call a specific object

let fruits = ["Apple", "Orange", "Watermelon", "Sampaloc", "Avocado"];
console.log (fruits);
console.log (fruits.length);
console.log (fruits[2]);

//Function

    //Function Declaration
    //function keyword
    //function name and parenthesis (holds the parameters)
    //curly braces - determine its block of codes; statements are written inside the code block


function greeting (firstName, lastName) //parameters a.k.a "arguments", "properties", "attributes"
                                        //named variable passed into a function
                                        //imports arguments into function
{

    console.log (`My name is ${firstName}, ${lastName}, it was nice to meet you`);

}

greeting ("Gerard", "Way"); //invoke function calls the function once
greeting ("Chester", "Bennington");

function product (a,b) {
    
        console.log ("Product:");
        return (a*b) //stops the execution of a function and returns a value
}

console.log(product(2,4));

function averageNum (num1, num2, num3) {
    return (num1+num2+num3)/3;
}

console.log (averageNum(1,2,3));

function convertNumbers (meters) {
    const m = 1000;
    console.log ("Result in km: ");
    return (meters/m)
}

console.log (convertNumbers(20));